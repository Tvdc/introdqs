<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailService
 *
 * @author tom
 */
class EmailService {

    public static function all() {
        require_once 'PersonDAOFactory.php';

        $persons = PersonDAOFactory::getDAO()->all();

        $emails = array();

        foreach ($persons as $person) {
            array_push($emails, $person->email);
        }

        return $emails;
    }

    public static function getStatusOpenTraining($openTraining) {
        require_once 'StatusDAOFactory.php';
        require_once 'PersonDAOFactory.php';

        $statusses = StatusDAOFactory::getDAO()->findByOpenTraining($openTraining);

        $emails = array();

        foreach ($statusses as $status) {
            array_push($emails, PersonDAOFactory::getDAO()->findById($status->personId)->email);
        }

        return $emails;
    }

    public static function getStatusHasFoundTeam($value) {
        require_once 'StatusDAOFactory.php';
        require_once 'PersonDAOFactory.php';

        $statusses = StatusDAOFactory::getDAO()->findByHasFoundTeam($value);
        
        $emails = array();

        foreach ($statusses as $status) {
            array_push($emails, PersonDAOFactory::getDAO()->findById($status->personId)->email);
        }

        return $emails;
    }
    
    public static function getPreferences($preferedTeams) {
        require 'PersonDAOFactory.php';
        
        $persons = PersonDAOFactory::getDAO()->findByPreference($preferedTeams);
        
        $emails = array();
        
        foreach($persons as $person) {
            array_push($emails, $person->email);
        }
        
        return $emails;
    }

}
