<?php

class PersonDAOFactory {

  public static function getDAO() : PersonDAO {
    require_once("PersonDAO.php");
    $personDAO = new PersonDAO();
    return $personDAO;
  }

}
