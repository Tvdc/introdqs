function Form(fields, language) {
    this.language = language || "nl";
    this.fields = fields;
}

Form.prototype.render = function () {


    for (var i = 0; i < this.fields.length; i++) {
        
        $("#" + this.fields[i].labelId).html(this.fields[i].labelText[this.language]);

        if (this.fields[i].placeholder) {
            document.getElementById(this.fields[i].inputId).placeholder = this.fields[i].placeholder[this.language]
        }

        if (this.fields[i].options) {
            var selectElem = document.getElementById(this.fields[i].inputId);
            selectElem.options.length = 0;
            for (var j = 0; j < this.fields[i].options.length; j++) {
                var option = document.createElement('option');
                option.text = this.fields[i].options[j][this.language];
                option.value = this.fields[i].options[j].value;
                selectElem.add(option);
            }
        }
    }

}

Form.prototype.validate = function () {
    var valid = true;
    for (var i = 0; i < this.fields.length; i++) {
        if (!document.getElementById(this.fields[i].inputId)) continue;
        
        var elem = $("#" + this.fields[i].inputId);
        elem.removeClass("invalid");

        if (this.fields[i].regex !== undefined && elem.val().match(this.fields[i].regex) === null) {
            elem.addClass("invalid");
            valid = false;
        }
    } 
    if (valid) {
        $.post("form.php", $('#dqsform').serialize()).done(function() {
            alert('succes');
            $('#dqsform').trigger("reset");
        }).fail(function (error) {
            alert("Failed: Server Error");
        }); 
    }
}

Form.prototype.setLanguageEnglish = function () {
    this.language = "en";
}

Form.prototype.setLanguageDutch = function () {
    this.language = "nl";
}

Form.prototype.isLanguageDutch = function () {
    return this.language === "nl";
}

Form.prototype.islanguageEnglish = function () {
    return this.language === "en";
}

