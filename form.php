<?php

session_start();

require_once("config.php");

if (isset($_POST['action']) && $_POST['action'] == "getLevels") {
    exit(json_encode(array("levels" => LEVELS)));
}

if (isset($_POST['action']) && $_POST['action'] == "getManTeams") {
    exit(json_encode(array("teams" => RECREATIONAL_MAN_TEAMS)));
}

if (isset($_POST['action']) && $_POST['action'] == "getWomenTeams") {
    exit(json_encode(array("teams" => RECREATIONAL_WOMEN_TEAMS)));
}

if (!areSetAndNotEmpty(REQUIRED_FORM_FIELDS, $_POST)) {
    http_response_code("400");
    exit(json_encode(array("response" => "Failure: Missing Parameters", "post" => var_dump($_POST))));
}

require_once("PersonService.php");

if ($_POST['action'] == "submit") {
    try {
        $persons = PersonService::storeNewPerson($_POST);
        http_response_code(200);
        exit();
    } catch (InvalidArgumentException $e) {
        http_response_code(400);
        exit();
    } catch (Exception $e) {
        http_response_code(500);
        exit();
    }
    exit;
}

function areSetAndNotEmpty($paramNames, $data) {
    foreach ($paramNames as $key) {
        if (!isset($data[$key]) || (empty($data[$key]) && $data[$key] != 0)) {
            return false;
        }
    }
    return true;
}
