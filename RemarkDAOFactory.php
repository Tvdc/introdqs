<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RemarkDAOFactory
 *
 * @author tom
 */
class RemarkDAOFactory {
    
    public static function getDAO() {
        require_once 'RemarkDAO.php';
        return new RemarkDAO();
    }
}
