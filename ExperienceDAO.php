<?php

class ExperienceDAO {

  public function save($experience) {

    assert($experience->personId != NULL);

    require_once("db.php");

    $db = DB::getConnection();

    $stmt = $db->prepare(
      "INSERT INTO experience (personId, formerClub, formerLevel, yearsOfExperience) VALUES
      (:personId, :formerClub, :formerLevel, :yearsOfExperience)");

    $stmt->execute(array(":personId" => $experience->personId,
                         ":formerClub" => $experience->formerClub,
                         ":formerLevel" => $experience->formerLevel,
                         ":yearsOfExperience" => $experience->yearsOfExperience
                       ));

  }

  public function update($experience) {

  }

  public function delete($experience) {
    require_once("db.php");

    $db = DB::getConnection();

    $stmt = $db->prepare("DELETE FROM experience WHERE experience.personId = :personId");

    $stmt->execute(array(":personId" => $experience->personId));
    
  }

  public function findById($personId) {
    require_once("db.php");
    require_once("Experience.php");

    $db = DB::getConnection();

    $stmt = $db->prepare(
      "SELECT * FROM experience WHERE experience.personId = :personId");
    $stmt->execute(array(":personId" => $personId));

    if ($stmt->rowcount() == 0) {
      return NULL;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $experience = new Experience($row);

    return $experience;
  }
  
  public function findByLevel($formerLevel) {
    require_once('db.php');
    require_once('Experience.php');
      
    $db = DB::getConnection();
     
    $stmt = $db->prepare("SELECT * FROM experience WHERE experience.formerLevel = :formerLevel");
      
    $stmt->execute(array(":formerLevel" => $formerLevel));
      
    $experiences = array();
      
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      array_push($experiences, new Experience($row));
    }
    
    return $experiences;      
  }

}
