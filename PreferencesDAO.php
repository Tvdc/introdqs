<?php

class PreferencesDAO {

    public function save($preferences) {
        require_once("db.php");

        assert($preferences->personId != NULL, "Preferences not linked to an person");

        $db = DB::getConnection();

        $stmt = $db->prepare("INSERT INTO preferences (personId, team) VALUES (:personId, :team)");

        foreach ($preferences->preferedTeams as $team) {
            $stmt->execute(array(":personId" => $preferences->personId,
                ":team" => $team));
        }
    }

    public function update($preferences) {
        $this->delete($preferences->personId);
        $this->save($preferences);
    }

    public function delete($personId) {
        require_once("db.php");

        assert(personId != NULL, "Preferences not linked to an person");

        $db = DB::getConnection();
        $stmt = $db->prepare("DELETE FROM preferences WHERE personId=:personId");
        $stmt->execute(array(":personId" => $personId));
    }

    public function findById($personId) {
        require_once("db.php");
        require_once("Preferences.php");

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT team FROM preferences WHERE preferences.personId = :personId");

        $stmt->execute(array(":personId" => $personId));

        $preferedTeams = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($preferedTeams, $row['team']);
        }

        $preferences = new preferences(array('personId' => $personId,
            'preferedTeams' => $preferedTeams));

        return $preferences;
    }

}
