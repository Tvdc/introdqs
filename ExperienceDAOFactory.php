<?php

class ExperienceDAOFactory {

  public static function getDAO() {
    require_once("ExperienceDAO.php");
    return new ExperienceDAO();
  }

}
