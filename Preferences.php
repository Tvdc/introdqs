<?php
class Preferences {

  public $personId;
  public $preferedTeams;

  public function __construct($data) {
    $this->setPersonId($data['personId']);
    $this->setPreferedTeams($data['preferedTeams']);
  }

  public function setPersonId($personId) {
    if (!preg_match("/^[0-9]+$/", $personId)) {
      throw new InvalidArgumentException("Preferences::setPersonId() : " . $personId);
    }
    $this->personId = $personId;
  }

  public function setPreferedTeams($preferedTeams) {
    require_once("config.php");
    foreach($preferedTeams as $team) {
      if (!in_array($team, TEAMS)) {
        throw new InvalidArgumentException("Preferences::setPreferedTeams() : " . $team);
      }
    }
    $this->preferedTeams = $preferedTeams;
  }

}
