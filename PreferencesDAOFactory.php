<?php

class PreferencesDAOFactory {

  public static function getDAO() {
    require_once("PreferencesDAO.php");
    return new PreferencesDAO();
  }

}
