<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

require_once("config.php");

if(!isset($_POST['action'])) {
    exit;
}

if ($_POST['action'] == 'login') {
    if (isset($_POST['username']) && !empty($_POST['username']) && 
        isset($_POST['password']) && !empty($_POST['password'])) {
        
        $userName = $_POST['username'];
        $password = $_POST['password'];
        
        if ($userName == ADMIN_USERNAME && $password == ADMIN_PASSWORD) {
            $_SESSION['authenticated'] = 'true';   
            header("Location: dashboard.html");
        } else {
            echo "<h1>Login Failed</h1>";
            exit;
        }
        
    }
    exit;
} 

if ($_POST['action'] == 'logout') {
    if ($_SESSION['authenticated'] == 'true') {
        unset($_SESSION['authenticated']);
        session_destroy();
        header("login.html");
        exit;
    }
    exit;
}

header("Location: login.html");
exit;