<?php

class DB {

    private static $db;

    protected function __construct() {
        
    }

    public static function getConnection() {

        if (empty(self::$db)) {
            require_once('config.php');
            self::$db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
          
        }
        return self::$db;
    }

}
