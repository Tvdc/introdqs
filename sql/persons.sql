CREATE TABLE `persons`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `firstName` varchar(100) NOT NULL,
    `initials` varchar(20) NULL,
    `infix` varchar(20) NULL,
    `familyName` varchar(100) NOT NULL,
    `gender` varchar(6) NOT NULL,
    `dateOfBirth` DATE NOT NULL,
    `phoneNumber` varchar(20) NOT NULL,
    `email` varchar(125) NOT NULL,
    `city` varchar(100) NULL,
    `institute` varchar(255) NOT NULL,
    `study` varchar(255) NOT NULL,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB;
