CREATE TABLE `preferences` (
    `personId` INT NOT NULL,
    `team` varchar(100) NOT NULL,

    KEY (`personId`, `team`)
) ENGINE=InnoDB;
