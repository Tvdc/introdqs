/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  tom
 * Created: Jul 26, 2017
 */

CREATE TABLE `status` (
    `personId` INT NOT NULL,
    `openTraining` varchar(100) NOT NULL,
    `hasFoundTeam` varchar(3) NOT NULL,

    PRIMARY KEY (`personId`)
) ENGINE=InnoDB;