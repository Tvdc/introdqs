/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  tom
 * Created: Jul 29, 2017
 */

CREATE TABLE `remarks` (
    `personId` INT NOT NULL,
    `remark` MEDIUMTEXT NOT NULL,

    PRIMARY KEY (`personId`)
) ENGINE=InnoDB;