CREATE TABLE `experience` (
    `personId` INT UNIQUE NOT NULL,
    `formerClub` varchar(100) NOT NULL,
    `formerLevel` varchar(100) NOT NULL,
    `yearsOfExperience` INT NOT NULL,

    PRIMARY KEY (`personId`)
) ENGINE=InnoDB;
