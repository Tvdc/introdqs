<?php

class Person {

    public $id;
    public $firstName;
    public $infix;
    public $familyName;
    public $initials;
    public $gender;
    public $dateOfBirth;
    public $phoneNumber;
    public $email;
    public $city;
    
    public $institute;
    public $study;

    public function __construct($data) {

        if (isset($data['id']))
            $this->id = $data['id'];
        
        $this->setFirstName($data['firstName']);
        // $this->setInitials($data['initials']);
        $this->setInfix($data['infix']);
        $this->setFamilyName($data['familyName']);
        $this->setGender($data['gender']);
        $this->setDateOfBirth($data['dateOfBirth']);
        $this->setPhoneNumber($data['phoneNumber']);
        $this->setEmail($data['email']);
        // $this->setCity($data['city']);
        $this->setInstitute($data['institute']);
        $this->setStudy($data['study']); 
    }

    public function setFirstName($firstName) {
        if (!is_string($firstName)) {
            throw new InvalidArgumentException("Person::setFirstName() : " . $firstName);
        }

        $this->firstName = htmlspecialchars(strtolower($firstName));
    }

    public function setInfix($infix) {
        if (!is_string($infix) && $infix != NULL && $infix != "") {
            throw new InvalidArgumentException("Person::setInfix() : " . $infix);
        }

        $this->infix = htmlspecialchars(strtolower($infix));
    }

    public function setFamilyName($familyName) {
        if (!is_string($familyName)) {
            throw new InvalidArgumentException("Person::setFamilyName() : " . $familyName);
        }

        $this->familyName = htmlspecialchars(strtolower($familyName));
    }

    public function setInitials($initials) {
        if (!is_string($initials) || !preg_match("/^[A-Za-z0-9](?:\s+[A-Za-z0-9])*$/", trim($initials))) {
            throw new InvalidArgumentException("Person::setInitials() : " . $initials);
        }

        $this->initials = htmlspecialchars(strtolower(trim($initials)));
    }

    public function setGender($gender) {
        $gender = strtolower($gender);
        if (!is_string($gender) || ($gender != "male" && $gender != "female")) {
            throw new InvalidArgumentException("Person::setGender() : " . $gender);
        }

        $this->gender = strtolower($gender);
    }

    public function setDateOfBirth($dateOfBirth) {
        if (!is_string($dateOfBirth)) {
            throw new InvalidArgumentException("Person::setDateOfBirth() : notstring" . $dateOfBirth);
        }

        $yyyymmdd = explode('-', $dateOfBirth);

        if (!preg_match("/^[0-9]{4}$/", $yyyymmdd[0]) || !preg_match("/^[0-9]{2}$/", $yyyymmdd[1]) || !preg_match("/^[0-9]{2}$/", $yyyymmdd[2])) {
            throw new InvalidArgumentException("Person::setDateOfBirth() : pregmatch" . $dateOfBirth);
        }

        // month, day, year
        if (!checkdate($yyyymmdd[1], $yyyymmdd[2], $yyyymmdd[0])) {
            throw new InvalidArgumentException("Person::setDateOfBirth() : notdate" . $dateOfBirth);
        }

        $this->dateOfBirth = $dateOfBirth;
    }

    public function setPhoneNumber($phoneNumber) {
        if (!is_string($phoneNumber)) {
            throw new InvalidArgumentException("Person::setPhoneNumber() : " . $phoneNumber);
        }
        
        if (!preg_match("/^\+?[0-9]{0,14}$/", $phoneNumber)) {
//        if (!preg_match("/^(?:(?:(?:\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/", $phoneNumber)) {
            throw new InvalidArgumentException("Person::setPhoneNumber() : " . $phoneNumber);
        }

        $this->phoneNumber = $phoneNumber;
    }

    public function setEmail($email) {
        if (!is_string($email)) {
            throw new InvalidArgumentException("Person::setEmail() : " . $email);
        }

        if (!preg_match("/^(?:[\w-]+(?:\.[\w-]+)*)@(?:(?:[\w-]+\.)*\w[\w-]{0,66})\.(?:[a-z]{2,6}(?:\.[a-z]{2})?)$/", $email)) {
            throw new InvalidArgumentException("Person::setEmail() : " . $email);
        }

        $this->email = strtolower($email);
    }

    public function setCity($city) {
        if (!is_string($city)) {
            throw new InvalidArgumentException("Person::setCity :" . $city);
        }

        $this->city = htmlspecialchars(strtolower($city));
    }

    public function setInstitute($institute) {
        require_once 'config.php';
        if (!is_string($institute) || !in_array(strtolower($institute), INSTITUTIONS)) {
            throw new InvalidArgumentException("Person::setInstitute : " . $institute);
        }

        $this->institute = strtolower($institute);
    }

    public function setStudy($study) {
        if (!is_string($study)) {
            throw new InvalidArgumentException("Person::setStudy : " . $study);
        }

        $this->study = htmlspecialchars(strtolower($study));
    }

    public function setId($id) {
        $id = intval($id);

        if (!is_int($id)) {
            throw new InvalidArgumentException("Person::setId : " . $id);
        }

        $this->id = $id;
    }

}