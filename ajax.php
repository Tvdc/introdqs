<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

/*
 * These functions are available only after authentication.
 */
if (!$_SESSION['authenticated'] == 'true') {
    http_response_code(401);
    exit();
}

if (!isset($_POST['action']) || empty($_POST['action'])) {
    http_response_code(400);
    exit();
}

if ($_POST['action'] == 'allPersons') {
    require_once 'PersonService.php';
    try {
        $persons = PersonService::all();
        http_response_code(200);
        exit(json_encode($persons));
    } catch (Exception $e) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'firstTeamsPersons') {
    require_once 'PersonService.php';
    require_once 'config.php';
    
    try {
        $personWrappers = PersonService::getByPreferences(SELECTION_TEAMS);
        http_response_code(200);
        exit(json_encode($personWrappers));
    } catch (Exception $e) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'updateStatus') {
    require_once 'StatusService.php';
    
    try {
        StatusService::updateStatus($_POST);
        http_response_code(200);
        exit();
    } catch (InvalidArgumentException $e) {
        http_response_code(400);
        exit();
    } catch (Exception $e) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'getUnknown') {
    require_once 'PersonService.php';
    try {
        $persons = PersonService::getByStatusOpenTraining("unknown");
        http_response_code('200');
        exit(json_encode($persons));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'getHasNotFoundTeam') {
    require_once 'PersonService.php';
    try {
        $persons = PersonService::getByStatusHasFoundTeam("no");
        http_response_code(200);
        exit(json_encode($persons));
    } catch (Exception $e) {
        exit();
    }
}

if ($_POST['action'] == 'getEmailsUnknown') {
    require_once 'EmailService.php';
    try {
        $emails = EmailService::getStatusOpenTraining("unknown");
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsPresent') {
    require_once 'EmailService.php';
    try {
        $emailsYes = EmailService::getStatusOpenTraining("yes");
        $emailsYesVega = Emailservice::getStatusOpenTraining("yes vega");
        $emails = array_merge($emailsYes, $emailsYesVega);
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsAbsent') {
    require_once 'EmailService.php';
    try {
        $emails = EmailService::getStatusOpenTraining("absent");
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}
if ($_POST['action'] == 'getEmailsall') {
    require_once 'EmailService.php';
    try {
        $emails = EmailService::all();
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsHasNotFoundTeam') {
    require_once 'EmailService.php';
    try {
        $emails = EmailService::getStatusHasFoundTeam("no");
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsHasFoundTeam') {
    require_once 'EmailService.php';
    try {
        $emails = EmailService::getStatusHasFoundTeam("yes");
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(501);
        exit();
    }
}

if ($_POST['action'] == 'fullDetails') {
    require_once 'PersonService.php';
    try {
        $personWrapper = PersonService::getById($_POST['id']);
        http_response_code(200);
        exit(json_encode($personWrapper));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'editPerson') {
    require_once 'PersonService.php';
    try {
        $person = PersonService::updatePerson($_POST);
        http_response_code(200);
        exit(json_encode($person));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsFirstManTeams') {
    require_once 'EmailService.php';
    require_once 'config.php';
    try {
        $emails = EmailService::getPreferences(SELECTION_MAN_TEAMS);
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsFirstWomenTeams') {
    require_once 'EmailService.php';
    require_once 'config.php';
    try {
        $emails = EmailService::getPreferences(SELECTION_WOMEN_TEAMS);
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

if ($_POST['action'] == 'getEmailsByTeams' && isset($_POST['teams'])) {
    require_once 'EmailService.php';
    require_once 'config.php';
    echo $_POST['teams'];
    try {
        $emails = EmailService::getPreferences($_POST['teams']);
        http_response_code(200);
        exit(json_encode($emails));
    } catch (Exception $ex) {
        http_response_code(500);
        exit();
    }
}

http_response_code(400);
exit();
