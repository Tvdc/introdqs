<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusDAOFactory
 *
 * @author tom
 */
class StatusDAOFactory {
    
    public static function getDAO() {
        require_once 'StatusDAO.php';
        return new StatusDAO();
    }
}
