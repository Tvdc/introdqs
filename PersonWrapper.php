<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PersonWrapper
 *
 * @author tom
 */
class PersonWrapper {

    public $person;
    public $experience;
    public $preferences;
    public $status;
    public $remark;

    public function __construct($data) {
        $this->setPerson($data['person']);
        $this->setExperience($data['experience']);

        if (isset($data['preferences']) && $data['preferences'] != NULL) {
            $this->setPreferences($data['preferences']);
        }

        $this->setStatus($data['status']);

        if (isset($data['remark']) && $data['remark'] != NULL) {
            $this->setRemark($data['remark']);
        }
    }

    public function setPerson($person) {
        require_once 'Person.php';

        if ($person instanceof Person) {
            $this->person = $person;
        } else {
            throw new InvalidArgumentException("PersonWrapper::setPerson() : " . $person);
        }
    }

    public function setExperience($experience) {
        require_once 'Experience.php';

        if ($experience instanceof Experience) {
            $this->experience = $experience;
        } else {
            throw new InvalidArgumentException("PersonWrapper::setExperience() : " . $experience);
        }
    }

    public function setPreferences($preferences) {
        require_once 'Preferences.php';

        if ($preferences instanceof Preferences) {
            $this->preferences = $preferences;
        } else {
            throw new InvalidArgumentException("PersonWrapper::setPreferences() : " . $preferences);
        }
    }

    public function setStatus($status) {
        require_once 'Status.php';

        if ($status instanceof Status) {
            $this->status = $status;
        } else {
            throw new InvalidArgumentException("PersonWrapper::setStatus() : " . $status);
        }
    }

    public function setRemark($remark) {
        require_once 'Remark.php';

        if ($remark instanceof Remark) {
            $this->remark = $remark;
        } else {
            throw new InvalidArgumentException("PersonWrapper::setRemark() : " . $remark);
        }
    }

}
