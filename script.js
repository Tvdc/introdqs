/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var form = new Form([
    {"labelId": "firstNameLabel", "labelText": {"en": "Firstname", "nl": "Voornaam"}, "inputId": "firstName", "regex": /^.+$/gi},
    {"labelId": "familyNameLabel", "labelText": {"en": "FamilyName", "nl": "Achternaam"}, "inputId": "familyName", "regex": /^.+$/gi},
    {"labelId": "infixLabel", "labelText": {"en": "Infix", "nl": "Tussenvoegsels"}, "inputId": "infix"},
    {"labelId": "genderLabel", "labelText": {"en": "Gender", "nl": "Geslacht"}, "inputId": "gender", "options": [{"value": "male", "en": "Male", "nl": "Man"}, {"value": "female", "en": "Female", "nl": "Vrouw"}]},
    {"labelId": "dateOfBirthLabel", "labelText": {"en": "Date Of Birth", "nl": "Geboorte Datum"}, "inputId": "dateOfBirth", "placeholder": {"en": "yyyy-mm-dd", "nl": "yyyy-mm-dd"}, "regex": /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/},
    {"labelId": "emailLabel", "labelText": {"en": "E-mail", "nl": "E-mail"}, "inputId": "email", "placeholder": {"en": "example@example.com", "nl": "voorbeeld@voorbeeld.nl"}, "regex":/^(?:[\w-]+(?:\.[\w-]+)*)@(?:(?:[\w-]+\.)*\w[\w-]{0,66})\.(?:[a-z]{2,6}(?:\.[a-z]{2})?)$/},
    {"labelId": "phoneNumberLabel", "labelText": {"en": "Phone Number", "nl": "Mobiele Nummer"}, "inputId": "phoneNumber", "placeholder": {"en": "0612345678", "nl": "0612345678"}, "regex": /^\+?[0-9]{6,14}$/g},
    {"labelId": "formerClubLabel", "labelText": {"en": "Former Club", "nl": "Vorige Club"}, "inputId": "formerClub"},
    {"labelId": "formerLevelLabel", "labelText": {"en": "Former Level", "nl": "Klasse Vorig Team"}, "inputId": "formerLevel", "options": []},
    {"labelId": "yearsOfExperienceLabel", "labelText": {"en": "Years Of Experience", "nl": "Ervaring In Jaren"}, "inputId": "yearsOfExperience", "regex": /^[0-9]{1,2}$/g },
    {"labelId": "firstTeamsLabel", "labelText": {"en": "Interrested in try-outs for H1, D1 or D2", "nl": "Geinteresseerd in selectie voor H1, D1, D2"}, "inputId": "firstTeams", "options": [{"value": "yes", "en": "Yes", "nl": "Ja"}, {"value": "no", "en": "No", "nl": "Nee"}]},
    {"labelId": "preferedTeamsLabel", "labelText": {"en": "Prefered Teams", "nl": "Voorkeur Teams"}, "inputId": "preferedTeams"},
    {"labelId": "instituteLabel", "labelText": {"en": "Institute", "nl": "Instituut"}, "inputId": "institute", "options": [{"value": "tue", "en": "TU/e", "nl": "TU/e"}, {"value": "fontys", "en": "Fontys", "nl": "Fontys"}, {"value": "other", "en": "Other", "nl": "Anders"}]},
    {"labelId": "studyLabel", "labelText": {"en": "Study", "nl": "Studie"}, "inputId": "study", "regex": /^([a-zA-Z\:\-\ ,])+$/},
    {"labelId": "openTrainingLabel", "labelText": {"en": "Opentraining + BBQ", "nl": "openTraining + BBQ"}, "inputId": "openTraining", "options": [{"value": "yes", "en": "Yes", "nl": "Ja"}, {"value": "yes vega", "en": "Yes, I am a vegitarian", "nl": "Ja, ik eet vega"}, {"value": "no", "en": "No", "nl": "Nee"}, {"value": "unknown", "en": "Don't know if I can be there", "nl": "Weet ik nog niet"}]},
    {"labelId": "remarkLabel", "labelText": {"en": "<h3>Remark</h3>Do you have any remarks or questions left? Put them down below.", "nl": "<h3>Opmerking</h3>Wil je nog iets kwijt of melden? Zet dat dan hieronder neer."}, "inputId": "remark"},
    {"labelId": "submit-button", "labelText": {"en": "Submit", "nl": "Inleveren"}, "inputId": "submit-button"},
    {"labelId": "openTrainingDescription", "labelText": {"en": "<h3>Opentraining</h3>There will be an opentraining + bbq on the 31st of August. All teams will be present and you'll be able to meet them. Participation costs 5 euros. ", "nl": "<h3>Opentraining</h3>31 augustus is er een opentraining + bbq. Hier zullen alle teams aanwezig zijn en kan je kennismaken met de teams. De kosten bedragen 5 euro."}},
    {"labelId": "experienceDescription", "labelText": {"en": "<h3>Hockey Experience</h3>Have you played hockey before? Then provide details of your experience below. If you have never played hockey before then leave the 'Former Club' field empty and fill in '0' at the 'Years Of Experience' field and 'nvt' at 'Former Level' field", "nl": "<h3>Hockey Ervaring</h3>Heb je al eens eerder gehockeyd? Geef dan hier je ervaring aan. Als je nog nooit hebt gehockeyd, laat 'Vorige Club' leeg en geef dan bij ervaring '0' aan en kies bij klasse vorig team 'nvt'."}},
    {"labelId": "studyDescription", "labelText": {"en": "<h3>Study</h3>Indicate at which institute you are studying and what you are studying.", "nl": "<h3>Studie</h3>Geef aan waar je studeerd en welke studie je doet."}},
    {"labelId": "personalDescription", "labelText": {"en": "<h3>Personal Info</h3>", "nl": "<h3>Persoonlijke Gegevens</h3>"}}
], "nl");

$('document').ready(function () {
        form.validate.bind(form);
        preferedTeamsMultipleSelect();
        formerLevelOptions();
        form.render();
    $('#submit-button').on("click", function () {
        form.validate();
    });
});

function toggleLanguage() {
    if (form.isLanguageDutch()) {
        form.setLanguageEnglish();
        form.render();
        document.getElementById('language-button').innerHTML = "Nederlands Formulier";
    } else {
        form.setLanguageDutch();
        form.render();
        document.getElementById('language-button').innerHTML = "English Form";
    }
}

function preferedTeamsMultipleSelect() {
    $.post("form.php", {"action": "getManTeams"})
            .done(function (response) {
                var manTeams = JSON.parse(response)['teams'];
                $.post("form.php", {"action": "getWomenTeams"})
                        .done(function (response) {
                            var womenTeams = JSON.parse(response)['teams'];
                            var teams = [manTeams, womenTeams];
                            var _select = $('<select>');
                            for (var i = 0; i < teams.length; i++) {
                                for (var j = 0; j < teams[i].length; j++) {
                                    _select.append($('<option></option>').val(teams[i][j]).html(teams[i][j]));
                                }
                            }
                            $('#preferedTeams').append(_select.html());
                            $('#preferedTeams').select2({
                                placeholder: "Teams",
                                theme: "bootstrap"
                            });
                        })
                        .fail(function (error) {
                            alert("Check de internet verbinding, kon geen verbinding met de server maken.");
                        });
            })
            .fail(function (error) {
                alert("Check de internet verbinding, kon geen verbinding met de server maken.");
            });
}

function formerLevelOptions() {
    $.post("form.php", {"action": "getLevels"}).done(function (response) {
        var levels = JSON.parse(response)['levels'];
        var html = [];
        for (var i = 0; i < levels.length; i++) {
            html.push('<option value="');
            html.push(levels[i]);
            html.push('">');
            html.push(levels[i]);
            html.push('</option>');
        }
        $('#formerLevel').html(html.join(""));
    }).fail(function (error) {
        alert("Check de internet verbinding, kon geen verbinding met de server maken.");
    });
}
