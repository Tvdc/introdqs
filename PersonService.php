<?php

class PersonService {

    public static function storeNewPerson($data) {
        require_once("PersonDAOFactory.php");
        require_once("Person.php");
        require_once("ExperienceDAOFactory.php");
        require_once("Experience.php");
        require_once("PreferencesDAOFactory.php");
        require_once("Preferences.php");
        require_once("StatusDAOFactory.php");
        require_once("Status.php");
        require_once("RemarkDAOFactory.php");
        require_once("Remark.php");
        require_once("db.php");

        try {
            
             DB::getConnection()->beginTransaction();
            
            $person = new Person($data);
            PersonDAOFactory::getDAO()->save($person);
            $data['personId'] = DB::getConnection()->lastInsertId();

            $experience = new Experience($data);
            ExperienceDAOFactory::getDAO()->save($experience);
            $data['preferedTeams'] = self::getPreferencesData($data);

            if (count($data['preferedTeams']) > 0) {
                $preferences = new Preferences($data);
                PreferencesDAOFactory::getDAO()->save($preferences);
            }

            $status = new Status(array("personId" => $data['personId'], "openTraining" => $data['openTraining'], "hasFoundTeam" => "no"));
            StatusDAOFactory::getDAO()->save($status);
            
            if (isset($data['remark']) && !empty(trim($data['remark']))) {
                $remark = new Remark($data);
                RemarkDAOFactory::getDAO()->save($remark);
            }

            DB::getConnection()->commit();
        } catch (PDOException $e) {
            DB::getConnection()->rollBack();
            throw $e;
        } catch (InvalidArgumentException $e) {
            DB::getConnection()->rollBack();
            throw $e;
        } catch (Exception $e) {
            DB:getConnection()->rollBack();
            throw $e;
        }
    }

    private static function getPreferencesData($data) {
        require_once 'config.php';

        $preferedTeams = array();

        if ($data['firstTeams'] == "yes") {
            if ($data['gender'] == "male") {
                $preferedTeams = array_merge($preferedTeams, SELECTION_MAN_TEAMS);
            } else {
                $preferedTeams = array_merge($preferedTeams, SELECTION_WOMEN_TEAMS);
            }
        }
        
        $teams = ($data['gender'] == "male") ? RECREATIONAL_MAN_TEAMS : RECREATIONAL_WOMEN_TEAMS;
        foreach ($teams as $team) {
            if (isset($data[$team]) || (isset($data['preferedTeams']) && in_array($team, $data['preferedTeams']))) {
                array_push($preferedTeams, $team);
            }
        }
        return $preferedTeams;
    }

    public static function all() {
        require_once("PersonDAOFactory.php");
        require_once("ExperienceDAOFactory.php");
        require_once("PreferencesDAOFactory.php");
        require_once("StatusDAOFactory.php");
        require_once("RemarkDAOFactory.php");
        require_once("PersonWrapper.php");

        try {
            $persons = PersonDAOFactory::getDAO()->all();
            $personWrappers = array();

            foreach ($persons as $person) {
                $experience = ExperienceDAOFactory::getDAO()->findById($person->id);
                $preferences = PreferencesDAOFactory::getDAO()->findBYId($person->id);
                $status = StatusDAOFactory::getDAO()->findById($person->id);
                $remark = RemarkDAOFactory::getDAO()->findById($person->id);

                array_push($personWrappers, new PersonWrapper(array("person" => $person, "experience" => $experience, "preferences" => $preferences, "status" => $status, "remark" => $remark)));
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $personWrappers;
    }

    public static function getByStatusOpenTraining($value) {
        require_once("PersonDAOFactory.php");
        require_once("ExperienceDAOFactory.php");
        require_once("PreferencesDAOFactory.php");
        require_once("StatusDAOFactory.php");
        require_once("RemarkDAOFactory.php");
        require_once("PersonWrapper.php");

        try {
            $statusses = StatusDAOFactory::getDAO()->findByOpenTraining($value);
            $personWrappers = array();

            foreach ($statusses as $status) {
                $experience = ExperienceDAOFactory::getDAO()->findById($status->personId);
                $preferences = PreferencesDAOFactory::getDAO()->findBYId($status->personId);
                $person = PersonDAOFactory::getDAO()->findById($status->personId);
                $remark = RemarkDAOFactory::getDAO()->findById($status->personId);

                array_push($personWrappers, new PersonWrapper(array("person" => $person, "experience" => $experience, "preferences" => $preferences, "status" => $status, "remark" => $remark)));
            }
        } catch (Exception $e) {
            throw new Exception("PersonService::getByOpenTraining() : " . $e);
        }

        return $personWrappers;
    }

    public static function getByStatusHasFoundTeam($value) {
        require_once("PersonDAOFactory.php");
        require_once("ExperienceDAOFactory.php");
        require_once("PreferencesDAOFactory.php");
        require_once("StatusDAOFactory.php");
        require_once("RemarkDAOFactory.php");
        require_once("PersonWrapper.php");

        try {
            $statusses = StatusDAOFactory::getDAO()->findByHasFoundTeam($value);
            $personWrappers = array();

            foreach ($statusses as $status) {
                $experience = ExperienceDAOFactory::getDAO()->findById($status->personId);
                $preferences = PreferencesDAOFactory::getDAO()->findBYId($status->personId);
                $person = PersonDAOFactory::getDAO()->findById($status->personId);
                $remark = RemarkDAOFactory::getDAO()->findById($status->personId);

                array_push($personWrappers, new PersonWrapper(array("person" => $person, "experience" => $experience, "preferences" => $preferences, "status" => $status, "remark" => $remark)));
            }
        } catch (Exception $e) {
            throw new Exception("PersonService::getByStatusHasFoundTeam() : " . $e);
        }

        return $personWrappers;
    }

    public static function getById($id) {
        require_once("PersonDAOFactory.php");
        require_once("ExperienceDAOFactory.php");
        require_once("PreferencesDAOFactory.php");
        require_once("StatusDAOFactory.php");
        require_once("RemarkDAOFactory.php");
        require_once("PersonWrapper.php");

        try {
            $person = PersonDAOFactory::getDAO()->findById($id);
            $experience = ExperienceDAOFactory::getDAO()->findById($person->id);
            $preferences = PreferencesDAOFactory::getDAO()->findBYId($person->id);
            $status = StatusDAOFactory::getDAO()->findById($person->id);
            $remark = RemarkDAOFactory::getDAO()->findById($person->id);

            $personWrapper = new PersonWrapper(array(
                "person" => $person,
                "experience" => $experience,
                "preferences" => $preferences,
                "status" => $status,
                "remark" => $remark
            ));
        } catch (Exception $e) {
            throw new Exception("PersonService::all() : " . $e);
        }

        return $personWrapper;
    }

    public static function updatePerson($data) {
        require_once("PersonDAOFactory.php");
        require_once("Person.php");
        require_once("ExperienceDAOFactory.php");
        require_once("Experience.php");
        require_once("PreferencesDAOFactory.php");
        require_once("Preferences.php");
        require_once("StatusDAOFactory.php");
        require_once("Status.php");
        require_once("RemarkDAOFactory.php");
        require_once("Remark.php");
        require_once("db.php");

        try {

            DB::getConnection()->beginTransaction();

            $person = new Person($data);
            PersonDAOFactory::getDAO()->update($person);

            $experience = new Experience($data);
            ExperienceDAOFactory::getDAO()->update($experience);

            $data['preferedTeams'] = array_map('strtoupper', array_map('trim', explode(',', $data['preferedTeams']) ) );
            $data['preferedTeams'] = self::getPreferencesData($data);
            
            $preferences = new Preferences($data);
            PreferencesDAOFactory::getDAO()->update($preferences);

            $status = new Status(array("personId" => $data['personId'], "openTraining" => $data['openTraining'], "hasFoundTeam" => "no"));
            StatusDAOFactory::getDAO()->update($status);

            $remark = new Remark($data);
            RemarkDAOFactory::getDAO()->update($remark);


            DB::getConnection()->commit();
            
            return self::getById($person->id);
            
        } catch (PDOException $e) {
            DB::getConnection()->rollBack();
            throw $e;
        } catch (InvalidArgumentException $e) {
            DB::getConnection()->rollBack();
            throw $e;
        } catch (Exception $e) {
            DB::getConnection()->rollBack();
            throw $e;
        }
    }

    public static function getByPreferences($preferedTeams) {
        require_once("PersonDAOFactory.php");
        require_once("ExperienceDAOFactory.php");
        require_once("PreferencesDAOFactory.php");
        require_once("StatusDAOFactory.php");
        require_once("RemarkDAOFactory.php");
        require_once("PersonWrapper.php");

        try {
            $persons = PersonDAOFactory::getDAO()->findByPreference($preferedTeams);
            $personWrappers = array();

            foreach ($persons as $person) {
                $experience = ExperienceDAOFactory::getDAO()->findById($person->id);
                $preferences = PreferencesDAOFactory::getDAO()->findBYId($person->id);
                $status = StatusDAOFactory::getDAO()->findById($person->id);
                $remark = RemarkDAOFactory::getDAO()->findById($person->id);

                array_push($personWrappers, new PersonWrapper(array("person" => $person, "experience" => $experience, "preferences" => $preferences, "status" => $status, "remark" => $remark)));
            }
        } catch (Exception $e) {
            throw new Exception("PersonService::getByPreferences() : " . $e);
        }

        return $personWrappers;
    }
    
}
