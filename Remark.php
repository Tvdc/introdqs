<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Remark {

    public $remark;
    public $personId;

    public function __construct($data) {
        $this->setPersonId($data['personId']);
        $this->setRemark($data['remark']);
    }

    public function setRemark($string) {
        if (!is_string($string)) {
            throw new InvalidArgumentException("Remark::setRemark : " . $remark);
        }
        $this->remark = htmlspecialchars($string);
    }

    public function setPersonId($personId) {
        if (!preg_match("/^[0-9]+$/", $personId)) {
            throw new InvalidArgumentException("Remark::setPersonId() : " . $personId);
        }
        $this->personId = (int) $personId;
    }

}
