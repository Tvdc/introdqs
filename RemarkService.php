<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RemarkService
 *
 * @author tom
 */
class RemarkService {
    
    public function all() {
        require_once 'RemarkDAOFactory.php';
        return RemarkDAOFactory::getDAO()->all();
    }
    
}
