<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusDAO
 *
 * @author tom
 */
class StatusDAO {
    
    public function update($status) {
        require_once 'db.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("UPDATE status SET openTraining= :openTraining, hasFoundTeam= :hasFoundTeam WHERE personId = :personId");
        $stmt->execute(array(":openTraining" => $status->openTraining, ":personId" => $status->personId, ":hasFoundTeam" => $status->hasFoundTeam));
    }
    
    public function save($status) {
        require_once 'db.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("INSERT INTO status (personId, openTraining, hasFoundTeam) VALUES (:personId, :openTraining, :hasFoundTeam)");
        $stmt->execute(array(":personId" => $status->personId, ":openTraining" => $status->openTraining, ":hasFoundTeam" => $status->hasFoundTeam));
        
    }
    
    public function delete($status) {
        require_once 'db.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("DELETE FROM status WHERE status.personId = :personId");
        $stmt->execute(array(":personId" => $status->personId));
        
    }
    
    public function findById($personId) {
        require_once 'db.php';
        require_once 'Status.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("SELECT * FROM status WHERE personId = :personId");
        $stmt->execute(array(":personId" => $personId));
        
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return new Status($data);        
    }
    
    public function findByOpenTraining($openTraining) {
        require_once 'db.php';
        require_once 'Status.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("SELECT * FROM status WHERE openTraining=:openTraining");
        $stmt->execute(array(":openTraining" => $openTraining));
        
        $statusses = array();
        
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($statusses, new Status($row));
        }
        
        return $statusses;
    }
    
    public function findByHasFoundTeam($value) {
        require_once 'db.php';
        require_once 'Status.php';
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("SELECT * FROM status WHERE hasFoundTeam=:hasFoundTeam");
        $stmt->execute(array(":hasFoundTeam" => $value));
        
        $statusses = array();
        
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($statusses, new Status($row));
        }
        
        return $statusses;
    }
    
}
