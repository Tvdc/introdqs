<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusService
 *
 * @author tom
 */
class StatusService {
    
    public static function updateStatus($data) {
        require_once 'StatusDAOFactory.php';
        require_once 'Status.php';
        
        $status = new Status($data);
        
        StatusDAOFactory::getDAO()->update($status);
    }
    
}
