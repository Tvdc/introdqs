<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status
 *
 * @author tom
 */
class Status {
    public $openTraining;
    public $hasFoundTeam;
    public $personId;
    
    function __construct($data) {
        
        $this->setPersonId($data['personId']);
        $this->setOpenTraining($data['openTraining']);
        $this->setHasFoundTeam($data['hasFoundTeam']);
        
    }
    
    public function setPersonId($personId) {
        if (!preg_match("/^[0-9]+$/", $personId)) {
            throw new InvalidArgumentException("Status::setPersonId() : " . $personId);
        }
        $this->personId = (int) $personId;
    }
    
    public function setOpenTraining($openTraining) {
        require_once 'config.php';
        if (!in_array(strtolower($openTraining), OPEN_TRAINING_STATUSSES)) {
            throw new InvalidArgumentException("Status::setOpenTraining : " . $openTraining);
        }
        $this->openTraining = strtolower($openTraining);
    }
    
    public function setHasFoundTeam($value) {
        require_once 'config.php';
        if (!in_array(strtolower($value), array("yes", "no"))) {
            throw new InvalidArgumentException("Status::setHasFoundTeam : " . $value);
        }
        $this->hasFoundTeam = strtolower($value);
    }
}
