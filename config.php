<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set( "Europe/Amsterdam" );  // http://www.php.net/manual/en/timezones.php
define( "DB_DSN", "mysql:host=localhost;dbname=intro;charset=utf8" );
define( "DB_USERNAME", "" );
define( "DB_PASSWORD", "" );
// define( "CLASS_PATH", "classes" );
// define( "TEMPLATE_PATH", "templates" );

define( "ADMIN_USERNAME", "" );
define( "ADMIN_PASSWORD", "" );

// require( CLASS_PATH . "/Person.php" );

// function handleException( $exception ) {
//   echo "Sorry, a problem occurred. Please try later.";
//   error_log( $exception->getMessage() );
// }
//
// set_exception_handler( 'handleException' );

define( "RECREATIONAL_MAN_TEAMS", array("H2", "H3", "H4", "H5", "H6") );

define( "RECREATIONAL_WOMEN_TEAMS", array("D3", "D4", "D5", "D6", "D7", "D8",
"D9", "D10") );

define( "SELECTION_MAN_TEAMS", array("H1") );

define( "SELECTION_WOMEN_TEAMS", array("D1", "D2") );

define( "SELECTION_TEAMS", array_merge(SELECTION_MAN_TEAMS, SELECTION_WOMEN_TEAMS) );

define( "TEAMS", array_merge(RECREATIONAL_MAN_TEAMS, RECREATIONAL_WOMEN_TEAMS, SELECTION_TEAMS) );

define( "REQUIRED_FORM_FIELDS", array("firstName", "familyName", "gender", "formerLevel",
  "dateOfBirth", "email", "phoneNumber", "firstTeams", "institute", "study", "yearsOfExperience") );

define ("OPTIONAL_FORM_FIELDS", array("initials", "formerClub", "remark") );

define( "FORM_FIELDS", array_merge(REQUIRED_FORM_FIELDS, OPTIONAL_FORM_FIELDS) );

define( "LEVELS", array("Weet ik niet meer", "nvt", "Jeugd: 4e klasse", "Jeugd: 3e klasse",
"Jeugd: 2e klasse", "Jeugd: 1e klasse", "Jeugd: super A / B",
"Jeugd: topklasse", "Jeugd: IDC", "Jeugd: landelijk", "Senioren: 4e klasse",
"Senioren: 3e klasse", "Senioren: 2e klasse", "Senioren: 1e klasse",
"Senioren: overgangsklasse",  "Senioren: hoofdklasse",
"Senioren: reserve 4e klasse", "Senioren: reserve 3e klasse",
"Senioren: reserve 2e klasse", "Senioren: reserve 1e klasse",
"Senioren: reserve overgangsklasse", "Senioren: reserve hoofdklasse") );

define ("OPEN_TRAINING_STATUSSES", array("no", "yes", "yes vega", "unknown") );

define( "INSTITUTIONS", array("tue", "fontys", "other") );
