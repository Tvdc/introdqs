<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RemarkDAO
 *
 * @author tom
 */
class RemarkDAO {

    public function save($remark) {
        require_once("db.php");

        $db = DB::getConnection();

        $stmt = $db->prepare("INSERT INTO remarks (personId, remark) VALUES (:personId, :remark)");

        $stmt->execute(array(":personId" => $remark->personId, ":remark" => htmlspecialchars($remark->remark)));
    }
    
    public function update($remark) {
        $this->delete($remark->personId);
        if (trim($remark->remark) != "") {
            $this->save($remark);
        }
    }

    public function delete($personId) {
        require_once 'db.php';

        $db = DB::getConnection();

        $stmt = $db->prepare("DELETE FROM remarks WHERE personId = :personId");

        $stmt->execute(array(":personId" => $personId));
    }

    public function all() {
        require_once 'Remark.php';
        require_once 'db.php';

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT * FROM remarks");

        $stmt->execute();

        $remarks = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($remarks, new Remark($row));
        }

        return $remarks;
    }

    public function findById($personId) {
        require_once 'Remark.php';
        require_once 'db.php';

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT * FROM remarks WHERE personId = :personId");

        $stmt->execute(array(":personId" => $personId));

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data) {
            return new Remark($data);
        } else {
            return NULL;
        }
    }

}
