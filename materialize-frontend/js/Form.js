function Form(fields, language) {
  this.language = language || "nl";
  this.fields = fields;
}

Form.prototype.render = function() {


  for (var i = 0; i < this.fields.length; i++) {

    $("#" + this.fields[i].labelId).html(this.fields[i].labelText[this.language]);

    if (this.fields[i].placeholder) {
      document.getElementById(this.fields[i].inputId).placeholder = this.fields[i].placeholder[this.language]
    }

    if (this.fields[i].options) {
      var selectElem = $("#" + this.fields[i].inputId)
        .find('option')
        .remove()
        .end()
      // selectElem.append($('<option></option>').val(' ').attr('id', 'default-' + this.fields[i].inputId).attr('disabled', 'disabled').attr('selected', 'selected'));
      for (var j = 0; j < this.fields[i].options.length; j++) {
        selectElem.append($('<option></option>').html(this.fields[i].options[j][this.language]).val(this.fields[i].options[j].value));
      }

    }
    /*
    if (this.fields[i]['option-placeholder']) {
      $('#default-' + this.fields[i].inputId).html(this.fields[i]['option-placeholder'][this.language]);
    }
    */
  }
  $('#gender').material_select();
  $('#institute').material_select();
  $('#firstTeams').material_select();
  $('#openTraining').material_select();

}

Form.prototype.validate = function() {
  console.log($('#dqsform').serialize());
  var valid = true;
  for (var i = 0; i < this.fields.length; i++) {
    if (!document.getElementById(this.fields[i].inputId)) continue;

    var elem = $("#" + this.fields[i].inputId);
    elem.removeClass("invalid");
    elem.parent().removeClass("invalid");
    elem.removeClass("valid");
    elem.parent().removeClass("valid");

    if (this.fields[i].regex !== undefined && elem.val().match(this.fields[i].regex) === null) {
      elem.addClass("invalid");
      elem.parent().addClass("invalid");
      if (valid) elem.focus();
      valid = false;
    } else {
      elem.addClass("valid");
      elem.parent().addClass("valid");
    }
  }
  if (valid) {
    $.post("form.php", $('#dqsform').serialize()).done(function() {
      alert('succes');
      $('#dqsform').trigger("reset");
    }).fail(function(error) {
      alert("Failed: Server Error");
    });
  }
}

Form.prototype.setLanguageEnglish = function() {
  this.language = "en";
}

Form.prototype.setLanguageDutch = function() {
  this.language = "nl";
}

Form.prototype.isLanguageDutch = function() {
  return this.language === "nl";
}

Form.prototype.islanguageEnglish = function() {
  return this.language === "en";
}
