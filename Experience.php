<?php

class Experience {

  public $personId;
  public $formerClub;
  public $formerLevel;
  public $yearsOfExperience;

  public function __construct($data) {
    require_once("config.php");

    $this->setPersonId($data['personId']);
    $this->setFormerClub($data['formerClub']);
    $this->setFormerLevel($data['formerLevel']);
    $this->setYearsOfExperience($data['yearsOfExperience']);
  }

  public function setPersonId($personId) {
    if (!preg_match("/^[0-9]+$/", $personId)) {
      throw new InvalidArgumentException("Experience::setPersonId() : " . $personId);
    }
    $this->personId = (int) $personId;
  }

  public function setFormerClub($formerClub) {
    if (!is_string($formerClub)) {
      throw new InvalidArgumentException("Experience::setFormerClub() : " . $formerClub);
    }
    $this->formerClub = $formerClub;
  }

  public function setFormerLevel($formerLevel) {
    if (!is_string($formerLevel)) {
      throw new InvalidArgumentException("Experience::setFormerLevel() : " . $formerLevel);
    }
    $this->formerLevel = $formerLevel;
  }

  public function setYearsOfExperience($yearsOfExperience) {
    if (!preg_match("/^[0-9]+$/", $yearsOfExperience)) {
      throw new InvalidArgumentException("Experience::setYearsOfExperience() : " . $yearsOfExperience);
    }
    $this->yearsOfExperience = $yearsOfExperience;
  }

}
