/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getAll() {
    $.post("ajax.php", {'action': 'allPersons'}).done(function (res) {
        var response = JSON.parse(res);
        var persons = response;
        var html = "";
        generateTable(persons);
    }).fail(function () {
        $("#wrapper").html("<br><br><center><h1> 401 Unauthorized </h1></center><br><a href='login.html'><h3>Click here to log in!</h3></a>");
    });
}

function getFirstTeams() {
    $.post("ajax.php", {'action': 'firstTeamsPersons'}).done(function (res) {
        var response = JSON.parse(res);
        var persons = response;
        var html = "";
        generateTable(persons);
    }).fail(function () {
        $("#wrapper").html("<br><br><center><h1> 401 Unauthorized </h1></center><br><a href='login.html'><h3>Click here to log in!</h3></a>");
    });
}

function getUnknown() {
    $.post("ajax.php", {"action": "getUnknown"}).done(function (res) {
        var response = JSON.parse(res);
        var persons = response;
        var html = "";
        generateTable(persons);
    });
}

function updateStatus(id) {
    $.post("ajax.php", {"action": "updateStatus", "personId": id, "openTraining": $('#openTraining-' + id).val(), "hasFoundTeam": $('#hasFoundTeam-' + id).val()}).done(function (res) {
 
    });
}

function logout() {
    $.post("login.php", {"action": "logout"}).done(function (res) {
        window.location.href = "login.html";
    });
}

function getHasNotFoundTeam() {
    $.post("ajax.php", {"action": "getHasNotFoundTeam"}).done(function (res) {
        var response = JSON.parse(res);
        var persons = response["response"];
        var html = "";
        generateTable(persons);
    });
}

$('document').ready(function () {
    getAll();
});

function requestEmails() {
    $.post("ajax.php", {"action": "getEmailsUnknown"}).done(function (res) {
        var emails = JSON.parse(res);
        alert(emails.join(', '));
    }).fail(function (response) {
        
    });
}

$('document').ready(function () {
    $("#email-dialog").dialog({
        autoOpen: false,
        width: 500,
        buttons: {
            OK: function () {
                getEmails();
                $(this).dialog("close");
            }
        },
        position: {
            my: "center",
            at: "center"
        }
    });
    $("#email-dialog-btn").click(function () {
        $("#email-dialog").dialog("open");
    });
});

$('document').ready(function () {
    $("#email-dialog-response").dialog({
        autoOpen: false,
        width: 500,
        buttons: {
            OK: function () {
                $(this).dialog("close");
            }
        },
        position: {
            my: "center",
            at: "center"
        }
    });

});

function getEmails() {
    var emailCategory = $("#request-email-select").val();
    ;
    var data = {"action": ""};

    switch (emailCategory) {
        case "unknown":
            data.action = "getEmailsUnknown";
            break;
        case "all":
            data.action = "getEmailsall";
            break;
        case "hasNotFoundTeam":
            data.action = "getEmailsHasNotFoundTeam";
            break;
        case "hasFoundTeam":
            data.action = "getEmailsHasFoundTeam";
            break;
        case "present":
            data.action = "getEmailsPresent";
            break;
        case "absent":
            data.action = "getEmailsAbsent";
            break;
        case "selection_man_teams":
            data.action = "getEmailsFirstManTeams";
            break;
        case "selection_women_teams":
            data.action = "getEmailsFirstWomenTeams";
            break;
    }

    $.post("ajax.php", data).done(function (res) {
        var emails = JSON.parse(res);
        var response = "";
        if (emails.length > 0) {
            response = emails.join(', ');
        } else {
            response = "Er is niemand die aan de criteria voldoet.";
        }
        $("#response").html(response);
        $("#email-dialog-response").dialog("open");
        $("#response").select();
    }).fail(function (response) {
        alert("Er is iets misgegaan :(");
    });

}

function generateTable(persons) {

    var columns = ["Naam", "Geboorte Datum", "E-mail", "Klasse", "Ervaring", "Voorkeursteams", "Open Training", "Team Gevonden", "Opmerking"]

    var table = '<table id="datatable"><thead><tr>';

    table += columns.map(function (a) {
        return "<th>" + a + "</th>";
    }).join(" ");
    table += '</tr></thead>';
    table += '<tbody>';

    for (var i = 0; i < persons.length; i++) {
        table += tableRow(persons[i]);
    }

    table += '</tbody>';
    table += '</table>';
    document.getElementById('testtable').innerHTML = table;
    sorttable.makeSortable(document.getElementById('datatable'));
    $('#datatable').find('tbody tr').click(function () {
        getFullDetails($(this).attr("personId"));
    });
    $('#datatable').find('tr td .openTraining, .foundTeam').click(function () {
        return false;
    });
    $('#datatable').find('thead tr').click(function () {
        return false;
    });
}

function tableRow(personWrapper) {
    var html = '<tr personId="' + personWrapper.person.id + '" id="trid-' + personWrapper.person.id + '">';
    html += "<td>" + personWrapper.person.firstName + " " + personWrapper.person.infix + " " + personWrapper.person.familyName + "</td>";
    html += '<td class="dateOfBirth">' + personWrapper.person.dateOfBirth + "</td>";
    html += "<td>" + personWrapper.person.email + "</td>";
    html += "<td>" + personWrapper.experience.formerLevel + "</td>";
    html += '<td class="yearsOfExperience">' + personWrapper.experience.yearsOfExperience + "</td>";
    html += "<td>" + personWrapper.preferences.preferedTeams.join(", ") + "</td>";
    html += '<td class="openTraining">' + openTraining(personWrapper.person.id, personWrapper.status.openTraining) + "</td>";
    html += '<td class="foundTeam">' + foundTeam(personWrapper.person.id, personWrapper.status.hasFoundTeam) + "</td>";
    html += (personWrapper.remark) ? '<td class="remark">' + personWrapper.remark.remark + "</td>" : "<td></td>";
    html += "</tr>";
    return html;
}
function tableDataElems(personWrapper) {
    var html = "<td>" + personWrapper.person.firstName + " " + personWrapper.person.infix + " " + personWrapper.person.familyName + "</td>";
    html += '<td class="dateOfBirth">' + personWrapper.person.dateOfBirth + "</td>";
    html += "<td>" + personWrapper.person.email + "</td>";
    html += "<td>" + personWrapper.experience.formerLevel + "</td>";
    html += '<td class="yearsOfExperience">' + personWrapper.experience.yearsOfExperience + "</td>";
    html += "<td>" + personWrapper.preferences.preferedTeams.join(", ") + "</td>";
    html += '<td class="openTraining">' + openTraining(personWrapper.person.id, personWrapper.status.openTraining) + "</td>";
    html += '<td class="foundTeam">' + foundTeam(personWrapper.person.id, personWrapper.status.hasFoundTeam) + "</td>";
    html += (personWrapper.remark) ? '<td class="remark">' + personWrapper.remark.remark + "</td>" : "<td></td>";
    html += "</tr>";
    return html;
}

function openTraining(id, statusOpenTraining) {
    var html = '<form class="tableForm" id="form-' + id + '" name="form-name-' + id + '">';
    html += "<input type=hidden name=\"personId\" value=\"" + id + "\"></input>";
    html += "<input type=hidden name=\"action\" value=\"updateStatus\"></input>";
    html += '<select class="openTraining" id="openTraining-' + id + '" name="openTraining" onchange="updateStatus(' + id + ')">';
    html += (statusOpenTraining == "yes") ? '<option selected value="yes">Aanwezig</option>' : '<option value="yes">Aanwezig</option>';
    html += (statusOpenTraining == "no") ? '<option selected value="no">Afwezig</option>' : '<option value="no">Afwezig</option>';
    html += (statusOpenTraining == "unknown") ? '<option selected value="unknown">Onbekend</option>' : '<option value="unknown">Onbekend</option>';
    html += (statusOpenTraining == "yes vega") ? '<option selected value="yes vega">Aanwezig vega</option>' : '<option value="yes vega">Aanwezig vega</option>';
    html += "</select></form>";

    return html;
}

function foundTeam(id, statusHasFoundTeam) {
    var html = '<select class="foundTeam" id="hasFoundTeam-' + id + '" name="hasFoundTeam" onchange="updateStatus(' + id + ')">';
    html += (statusHasFoundTeam == "yes") ? '<option selected value="yes">Geplaatst</option>' : '<option value="yes">Geplaatst</option>';
    html += (statusHasFoundTeam == "no") ? '<option selected value="no">Nog Niet Geplaatst</option>' : '<option value="no">Nog Niet Geplaatst</option>';
    html += "</select>";

    return html;
}

function getFullDetails(personId) {
    $.post("ajax.php", {"action": "fullDetails", "id": personId}).done(function (res) {
        var personWrapper = JSON.parse(res);
        var html = "";

        html += personEditForm(personWrapper);

        $("#full-details-dialog").html(html);
        $("#full-details-dialog").dialog("open");
    }).fail(function (res) {
        alert("Er is iets misgegaan bij het opvragen van de gegevens... :(");
    });
}

function personEditForm(personWrapper) {
    var html = '<form id="editForm">';
    html += '<input type="hidden" name="id" value="' + personWrapper.person.id + '" />';
    html += '<input type="hidden" name="personId" value="' + personWrapper.person.id + '" />';
    html += '<input type="hidden" name="action" value="editPerson" />';
    html += formLine('Voornaam', '<input type="text" name="firstName" value="' + personWrapper.person.firstName + '"/>');
    html += formLine('Tussen Voegsels', '<input type="text" name="infix" value="' + personWrapper.person.infix + '"/>');
    html += formLine('Achternaam', '<input type="text" name="familyName" value="' + personWrapper.person.familyName + '"/>');
    html += formGenderLine(personWrapper.person.gender);
    html += formLine('Geboorte Datum', '<input type="text" name="dateOfBirth" placeholder="dd-mm-yyyy" value="' + personWrapper.person.dateOfBirth + '"/>');
    html += formLine('E-mail', '<input type="text" name="email" value="' + personWrapper.person.email + '"/>');
    html += formLine('Mobiele Nummer', '<input type="text" name="phoneNumber" value="' + personWrapper.person.phoneNumber + '"/>');
    html += formLine('Vorige Club', '<input type="text" name="formerClub" value="' + personWrapper.experience.formerClub + '"/>');
    html += formLine('Klasse Vorig Team', '<input type="text" name="formerLevel" value="' + personWrapper.experience.formerLevel + '"/>');
    html += formLine('Ervaring', '<input type="text" name="yearsOfExperience" value="' + personWrapper.experience.yearsOfExperience + '"/>');
    html += formFirstTeamsLine(personWrapper.preferences.preferedTeams);
    html += formLine('Voorkeur Teams', '<input type="text" name="preferedTeams" value="' + personWrapper.preferences.preferedTeams + '"/>');
    html += formInstituteLine(personWrapper.person.institute);
    html += formLine('Studie', '<input type="text" name="study" value="' + personWrapper.person.study + '"/>');
    html += formOpenTrainingLine(personWrapper.status.openTraining);
    html += (personWrapper.remark) ? formRemark(personWrapper.remark.remark) : formRemark("");
    return html;
}

function formLine(name, formelem) {
    return '<div class="line"><label>' + name + ' : &nbsp </label><div class="input">' + formelem + '</div></div>';
}

function formGenderLine(gender) {
    var html = '<select name="gender">';
    html += (gender == "male") ? '<option selected value="male">Man</option>' : '<option value="male">Man</option>';
    html += (gender == "female") ? '<option selected value="female">Vrouw</option>' : '<option value="female">Vrouw</option>';
    html += '</select>';
    return formLine("Geslacht", html + "</br>");
}

function formFirstTeamsLine(preferedTeams) {
    var html = '<select name="firstTeams">';
    html += (preferedTeams.indexOf("H1") >= 0 || preferedTeams.indexOf("D1") >= 0) ? '<option selected value="yes">Ja</option>' : '<option value="yes">Ja</option>';
    html += (preferedTeams.indexOf("H1") < 0 && preferedTeams.indexOf("D1") < 0) ? '<option selected value="no">Nee</option>' : '<option value="no">Nee</option>';
    html += '</select>';
    return formLine("Interesse H1, D1 of D2", html + "</br>");
}

function formOpenTrainingLine(statusOpenTraining) {
    var html = '<select name="openTraining">';
    html += (statusOpenTraining == "yes") ? '<option selected value="yes">Aanwezig</option>' : '<option value="yes">Aanwezig</option>';
    html += (statusOpenTraining == "no") ? '<option selected value="no">Afwezig</option>' : '<option value="no">Afwezig</option>';
    html += (statusOpenTraining == "unknown") ? '<option selected value="unknown">Onbekend</option>' : '<option value="unknown">Onbekend</option>';
    html += (statusOpenTraining == "yes vega") ? '<option selected value="yes vega">Aanwezig vega</option>' : '<option value="yes vega">Aanwezig vega</option>';
    html += "</select>";
    return formLine("Open Training", html + "</br>");
}

function formInstituteLine(institute) {
    var html = '<select name="institute">';
    html += (institute == "tue") ? '<option selected value="tue">TU/e</option>' : '<option value="tue">TU/e</option>';
    html += (institute == "fontys") ? '<option selected value="fontys">Fontys</option>' : '<option value="fontys">Fontys</option>';
    html += (institute == "other") ? '<option selected value="other">Andere</option>' : '<option value="other">Andere</option>';
    html += "</select>";
    return formLine("Onderwijs Instituut", html + "</br>");
}

function formRemark(remark) {
    var html = '<br><textarea name="remark" id="formRemark" placeholder="Typ hier een opmerking...">' + remark + "</textarea>";
    return html;
}

function updatePerson(dialog) {
    var data = $('#editForm').serializeArray()
            .reduce(function(a, x) { a[x.name] = x.value; return a; }, {});
    
    $.post("ajax.php", data).done(function (response) {
        var personWrapper = JSON.parse(response);
        $('#trid-' + personWrapper.person.id).html(tableDataElems(personWrapper));
        dialog.dialog("close");
    }).fail(function () {
        alert("Er is iets misgegaan... :(");
    });
}

$('document').ready(function () {
    $("#full-details-dialog").dialog({
        autoOpen: false,
        width: 537,
        buttons: {
            OK: function () {
                updatePerson($(this));
            }
        },
        position: {
            my: "center",
            at: "center"
        }
    });

});