<?php

class PersonDAO {

    public function update($person) {
        require_once('db.php');
        
        $db = DB::getConnection();
        
        $stmt = $db->prepare("UPDATE persons SET firstName=:firstName, "
                . "initials=:initials, infix=:infix, "
                . "familyName=:familyName, gender=:gender, "
                . "dateOfBirth=:dateOfBirth, phoneNumber=:phoneNumber, "
                . "email=:email, city=:city, institute=:institute, "
                . "study=:study WHERE id=:id");
        
        $stmt->execute(array(
            ":id" => $person->id,
            ":firstName" => $person->firstName,
            ":initials" => $person->initials,
            ":infix" => $person->infix,
            ":familyName" => $person->familyName,
            ":gender" => $person->gender,
            ":dateOfBirth" => $person->dateOfBirth,
            ":phoneNumber" => $person->phoneNumber,
            ":email" => $person->email,
            ":city" => $person->city,
            ":institute" => $person->institute,
            ":study" => $person->study
                )
        );
    }

    public function delete($person) {
        require_once("db.php");

        $db = DB::getConnection();

        $stmt = $db->prepare("DELETE FROM persons WHERE persons.id = :id");

        $stmt->execute(array(":id" => $person->id));
    }

    public function findById($id) {
        require_once("db.php");
        require_once("Person.php");

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT * FROM persons WHERE persons.id = :id");
        $stmt->execute(array(":id" => $id));

        if ($stmt->rowcount() == 0) {
            return NULL;
        }

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $person = new Person($row);

        return $person;
    }

    public function all() {
        require_once("db.php");
        require_once("Person.php");

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT * FROM persons");
        $stmt->execute();

        $persons = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($persons, new Person($row));
        }

        return $persons;
    }

    public function save($person) {
        require_once("db.php");

        assert($person->id == NULL, 'PersonDAO::save() : $person already has an $id');

        $db = DB::getConnection();

        $stmt = $db->prepare("INSERT INTO persons (firstName, initials, infix,
      familyName, gender, dateOfBirth, phoneNumber, email, city, institute, study) VALUES
      (:firstName, :initials, :infix, :familyName, :gender, :dateOfBirth,
        :phoneNumber, :email, :city, :institute, :study )");

        $stmt->execute(array(
            ":firstName" => $person->firstName,
            ":initials" => $person->initials,
            ":infix" => $person->infix,
            ":familyName" => $person->familyName,
            ":gender" => $person->gender,
            ":dateOfBirth" => $person->dateOfBirth,
            ":phoneNumber" => $person->phoneNumber,
            ":email" => $person->email,
            ":city" => $person->city,
            ":institute" => $person->institute,
            ":study" => $person->study
                )
        );

        $personId = $db->lastInsertId();

        return $personId;
    }

    public function findByLevel($level) {
        require_once('db.php');
        require_once('Person.php');

        $db = DB::getConnection();

        $stmt = $db->prepare("SELECT id, firstName, initials, infix, familyName,"
                . " gender, dateOfBirth, phoneNumber, email, city, institute, "
                . "study"
                . " FROM persons LEFT JOIN experience ON persons.id = experience.personId "
                . "WHERE experience.formerLevel = :formerLevel");

        $stmt->execute(array(":formerLevel" => $level));

        $persons = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($persons, new Person($row));
        }

        return $persons;
    }

    public function findByStatus($openTraining) {
        require_once('config.php');
        require_once('db.php');
        require_once('Person.php');
        require_once('Status.php');

        if (!in_array($openTraining, OPEN_TRAINING_STATUSSES)) {
            throw new InvalidArgumentException("PersonDAO::findByStatus " . $status);
        }

        $db = DB::getConnection();

        $stmt = $db->prepare(""
                . "SELECT * "
                . "FROM persons LEFT JOIN status ON persons.id = status.personId "
                . "WHERE status.openTraining = :openTraining");

        $stmt->execute(array(":openTraining" => $openTraining));

        $persons = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $person = new Person($row);
            $status = new Status($row);
            $person->setStatus($status);
            array_push($persons, $person);
        }

        return $persons;
    }

    public function findByPreference($preferences) {
        require_once('db.php');
        require_once('Person.php');

        $sql = array();
        $params = array();

        foreach ($preferences as $team) {
            if (!in_array($team, TEAMS)) {
                throw new InvalidArgumentException("PersonDAO::findByPreference() : " . $preferences);
            } else {
                array_push($sql, "preferences.team = :$team");
                $params[":" . $team] = $team;
            }
        }

        $db = DB::getConnection();

        $sql = implode(" OR ", $sql);

        $sql = "SELECT DISTINCT * from preferences WHERE " . $sql;

        $stmt = $db->prepare($sql);

        $stmt->execute($params);

        $persons = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($persons, $this->findById($row['personId']));
        }

        return $persons;
    }

    public function toMYSQLDate($ddmmyyyy) {
        $arr = explode('-', $ddmmyyyy);
        return sprintf("%04d-%02d-%02d", $arr[2], $arr[1], $arr[0]);
    }

    public function fromMYSQLDate($yyyymmdd) {
        $arr = explode('-', $yyyymmdd);
        return sprintf("%02d-%02d-%04d", $arr[2], $arr[1], $arr[0]);
    }

}
